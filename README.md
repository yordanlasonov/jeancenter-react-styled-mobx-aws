## Install

```bash
npm install or yarn
npm run start:dev or yarn run start:dev
```

Your browser should open at

```
http://localhost:3000
```
