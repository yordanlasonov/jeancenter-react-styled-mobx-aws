import React from 'react';
import styled from 'styled-components';

const StyledFooter = styled.div`
  z-index: 2;
  position: fixed;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  background-color: #efefef;
  text-align: center;
`;

function Footer() {
  return (
    <StyledFooter>
      Jeans Center © 2018
    </StyledFooter>
  );
}

export default Footer;