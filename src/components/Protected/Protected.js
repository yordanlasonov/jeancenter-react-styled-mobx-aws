import React from 'react';

import SuperAdminPanel from '../SuperAdminPanel';
import UserSearch from '../UserSearch';
import Footer from '../Footer';

import { Page, MessageBox } from '../Layout';
import { observer } from "mobx-react";
import { authStore, languageStore } from '../../stores';

const lang = languageStore.language;

class ProtectedComponent extends React.Component {
  componentDidMount() {
    authStore.getAuth();
  }
  render() {
    return (
      <Page title={lang.search.title}>
        <SuperAdminPanel />
        <UserSearch />
        <MessageBox />
        <Footer />
      </Page>
    );
  };
}

export default observer(ProtectedComponent);