import React from 'react';
import { VerifyContact } from 'aws-amplify-react';


class MyVerifyContact extends VerifyContact {
  componentDidUpdate(){
    if (this.props.authState === 'verifyContact'){
      window.location.reload();
    }
  }
  
  render() {
    if (this.props.authState !== 'verifyContact') return false;
    return (
      <div></div>
    );
  }
}

export default MyVerifyContact;