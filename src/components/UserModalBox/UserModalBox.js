import React, { Component } from 'react';

import { languageStore, modalBoxStore } from '../../stores';
import { observer } from 'mobx-react';

import UserModalBoxContent from '../UserModalBoxContent';

import styled from 'styled-components';

const lang = languageStore.language.modalBox;

const ModalBox = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.4);
  z-index: 3;
  .user-info {
    text-align: center;
    margin-top: 0;
  }
  .modalbox-content {
    width: 40%;
    min-width: 600px;
    position: absolute;
    top: 50%;
    left: 50%;
    background-color: white;
    transform: translate(-50%, -50%);
    padding: 20px 30px 50px;
  }
  .modalbox-close {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 5px;
    top: 5px;
  }
  .modalbox-content label {
    display: block;
    margin-top: 10px;
    margin-bottom: 5px;
    font-size: 16px;
  }
  .modalbox-content input {
    font-size: 16px;
  }
`;

class UserModalBox extends Component {
  constructor(props) {
    super(props);
    this.close = this.close.bind(this);
  }

  close() {
    modalBoxStore.close();
  }

  render() {
    return (
      <div>
        {modalBoxStore.isShown && (
          <ModalBox>
            <div className="modalbox-content">
              <span className="modalbox-close" onClick={this.close}>
                ╳
              </span>
              <h2 className="user-info">{lang.title}</h2>
              <UserModalBoxContent />
            </div>
          </ModalBox>
        )}
      </div>
    );
  }
}

export default observer(UserModalBox);
