import React, { Component } from 'react';

import { Tabs, Tab } from '@material-ui/core/';

import {languageStore, userProfileStore, userAddressesStore} from '../../stores';
import { observer } from "mobx-react"; 

import UserModalBoxAddresses from '../UserModalBoxAddresses';
import UserModalBoxNewAddress from '../UserModalBoxNewAddress';

const lang = languageStore.language.modalBox.addresses;


class UserModalBoxAddressesContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: 'last'
    };

    this.handleTabChange = this.handleTabChange.bind(this);
  }

  componentDidMount(){
    if(userAddressesStore.userAddresses[0]){
      this.setState({tab:userAddressesStore.userAddresses[0].uid});
    }
    else{
      this.setState({tab:'last'});
    }
  }
  
  
  handleTabChange = (event, value) => {
    this.setState({
      tab: value
    });
  };

  render() {
    let user = userProfileStore.userProfile;
    let addresses = userAddressesStore.userAddresses;
    return (
      <div>
        <Tabs
          style={{backgroundColor:'#e8e8e8'}}
          value={this.state.tab}
          scrollable
          indicatorColor='primary'
          textColor='secondary'
          onChange={this.handleTabChange}>
          {addresses.map((addresses, index) =>
            <Tab 
              key={index} 
              value={addresses.uid} 
              label={`${lang.addressTab} ${index + 1}`}
            />
          )}
          <Tab value='last' label={lang.addAddressTab} />
        </Tabs>
        {
          addresses.map((addresses, index) =>
            <div key={index}>
              {(this.state.tab === addresses.uid) &&
                <UserModalBoxAddresses userId={user.id} addresses={addresses} index={index} />
              }
            </div>
          )
        }
        {(this.state.tab === 'last') &&
          <React.Fragment>
            <UserModalBoxNewAddress />
          </React.Fragment>
        }
      </div>
    );
  }
}

export default observer(UserModalBoxAddressesContent);
