import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { InputsWrapper, ButtonsModalBox, FormInput, FormOption, FormCheckbox } from '../Layout/';

import { languageStore, userProfileStore, userAddressesStore, userNewAddressesStore }  from '../../stores';
import { observer } from "mobx-react"; 

import genderOptions from '../../lists/gender.json';
import countryOptions from '../../lists/country.json';

const lang = languageStore.language.modalBox.addresses;

class UserModalBoxAddresses extends Component {
  static propTypes = {
    index: PropTypes.number
  }
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  };

  handleSubmit = async () => {
    await userNewAddressesStore.handleSubmit(userProfileStore.userProfile.id);
    userAddressesStore.refetchUserAddresses();
  }

  handleChange(event) {
    userNewAddressesStore.handleChange(event);
  }

  render() {
    let user = userNewAddressesStore.userNewAddresses;
    return (
      <div>
        <InputsWrapper>
          <FormCheckbox label={lang.addDefaultAddress} name='defaultAddress' value={user.defaultAddress} onChange={this.handleChange}/>
          <FormInput label={lang.firstName} name='name' value={user.name} onChange={this.handleChange} />
          <FormInput label={lang.familyName} name='familyName' value={user.familyName} onChange={this.handleChange} />
          <FormInput label={lang.phoneNumber} name='phoneNumber' value={user.phoneNumber} onChange={this.handleChange} />
          <FormOption label={lang.country} name='country' value={user.country} options={countryOptions} onChange={this.handleChange} />
          <FormOption label={lang.gender} name='gender' value={user.gender} options={genderOptions} onChange={this.handleChange} />
        </InputsWrapper>
        <InputsWrapper>
          <FormInput label={lang.city} name='city' value={user.city} onChange={this.handleChange} />
          <FormInput label={lang.streetName} name='streetName' value={user.streetName} onChange={this.handleChange} />
          <FormInput label={lang.houseNumber} name='houseNumber' value={user.houseNumber} onChange={this.handleChange} />
          <FormInput label={lang.postalCode} name='postalCode' value={user.postalCode} onChange={this.handleChange} />
          <FormInput label={lang.addition} name='addition' value={user.addition} onChange={this.handleChange} />
        </InputsWrapper>
        <ButtonsModalBox
          submitLabel={lang.addAddressBtn}
          onSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}

export default observer(UserModalBoxAddresses);