
import React from "react";
import PropTypes from "prop-types";

import styled from "styled-components";

const LoyaltyStyled = styled.div`
  margin-bottom:20px;
  font-weight: bold;
`;


const LoyaltyCardNumber = ({loyaltycardnumber}) => (
  <LoyaltyStyled>
    JC Friend nummer : {loyaltycardnumber}
  </LoyaltyStyled>
);

LoyaltyCardNumber.propTypes = {
  loyaltycardnumber: PropTypes.string
};

export default LoyaltyCardNumber;