
import React from "react";
import PropTypes from "prop-types";
import styled from 'styled-components';


const InputWrapper = styled.div`
  height:56px;
  input{
    font-size: 16px;
    height:26px;
    width: 250px;
  }
`;

const FormInput = ({ label, name, value, onChange }) => (
  <InputWrapper>
    <label>{label}</label>
    <input type='text' name={name} value={value} onChange={onChange}/>
  </InputWrapper>
);

FormInput.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func
};

export default FormInput;