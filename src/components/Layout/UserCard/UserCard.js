
import React from "react";
import PropTypes from "prop-types";

import styled from "styled-components";
import Card from '@material-ui/core/Card';


const StyledCard = styled(Card)`
  display: flex;
  flex-direction:row;
  align-content: center;
  background-color: white;
  cursor: pointer;
  width:50%;
  padding: 20px 10px;
  margin-top: 10px;
  line-height:25px;
  img{
    margin-right:20px;
  }
  &:hover{
    background-color: #ededed;
  }
`;

const UserCard = ({ user, onClick }) => (
  <StyledCard onClick={onClick}>
    <div>
      <img
        width="50"
        height="50"
        alt="DefaultImage"
        src="default-profile-img.png"
      />
    </div>
    <div>
      <span>
        {user.name} {user.familyName}
      </span>
      <br />
      <span>{user.email}</span>
    </div>
  </StyledCard>
);

UserCard.propTypes = {
  user: PropTypes.object,
  onClick: PropTypes.func
};

export default UserCard;