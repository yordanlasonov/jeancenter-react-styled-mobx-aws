
import React from "react";
import PropTypes from "prop-types";

import styled from "styled-components";

const StyledCheckbox = styled.div`
  display:flex;
  align-items:center;
  height:56px;
`;

const FormCheckbox = ({ label, name, value, onChange }) => (
  <StyledCheckbox>
    <label>
      <input type='checkbox' name={name} checked={value} onChange={onChange}/>
      {label}
    </label>
  </StyledCheckbox>
);

FormCheckbox.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.bool,
  onChange: PropTypes.func
};

export default FormCheckbox;