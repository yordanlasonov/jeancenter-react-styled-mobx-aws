
import React from "react";
import PropTypes from "prop-types";

import styled from "styled-components";

const IsActiveStyled = styled.div`
  
`;

const Active = styled.h3`
  color: #80b93c;
`;

const NotActive = styled.h3`
  color: #ed0000;
`;

const IsActiveUser = (props) => (
  <IsActiveStyled>
    {props.enabled ? <Active>Active</Active> : <NotActive>Not Active</NotActive>}
  </IsActiveStyled>
);

IsActiveUser.propTypes = {
  enabled: PropTypes.bool
};

export default IsActiveUser;