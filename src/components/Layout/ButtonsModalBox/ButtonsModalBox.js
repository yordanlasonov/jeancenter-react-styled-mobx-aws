import React from 'react';
import styled from 'styled-components';
import PropTypes from "prop-types";

import Button from '@material-ui/core/Button';

const ButtonsWrapper = styled.div`
  margin-top:50px;
  text-align:right;
  > button{
    margin-right:10px;
  }
  > button:last-of-type{
    margin-right:0;
  }
`;
const DangerButton = styled(Button)`
&&{
    background-color: #ed0000;
    color: white;
    &:hover{
      background-color: #9b0000;
    }
  }
`;
const SecondaryButton = styled(Button)`
&&{
    background-color: #7f7f7f;
    color: white;
    &:hover{
      background-color: #5b5b5b;
    }
  }
`;

const ButtonModalBox = ({ onSubmit, onDelete, onSecondaryClick, submitLabel, dangerLabel, secondaryLabel }) => (
  <div>
    <ButtonsWrapper>
      {dangerLabel &&
        <DangerButton
          type='submit'
          variant='contained'
          onClick={onDelete}>
          {dangerLabel}
        </DangerButton>
      }      
      {secondaryLabel &&
      <SecondaryButton
        type='submit'
        variant='contained'
        color='primary'
        onClick={onSecondaryClick}>
        {secondaryLabel}
      </SecondaryButton>
      }
      {submitLabel &&
        <Button
          type='submit'
          variant='contained'
          color='primary'
          onClick={onSubmit}>
          {submitLabel}
        </Button>
      }

    </ButtonsWrapper>
  </div>
);

ButtonModalBox.propTypes = {
  onSubmit: PropTypes.func,
  onDelete: PropTypes.func,
  onSecondaryClick: PropTypes.func,
  dangerLabel: PropTypes.string,
  submitLabel: PropTypes.string,
  secondaryLabel: PropTypes.string
};

export default ButtonModalBox;