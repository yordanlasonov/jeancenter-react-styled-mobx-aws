
import React from "react";
import PropTypes from "prop-types";

import NativeSelect from '@material-ui/core/NativeSelect';

import styled from "styled-components";

const InputWrapper = styled.div`

`;

const StyledSelect = styled(NativeSelect)`
  width:100%;
`;

const FormOption = ({ label, name, value, onChange, options }) => (
  <InputWrapper>
    <label>{label}</label>
    <StyledSelect
      name={name}
      value={value}
      onChange={onChange}
    >
      {options.map(options =>
        <option key={options.value} value={options.value}>{options.name}</option>
      )};
    </StyledSelect>
  </InputWrapper>
);

FormOption.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  options: PropTypes.array,
  onChange: PropTypes.func
};

export default FormOption;