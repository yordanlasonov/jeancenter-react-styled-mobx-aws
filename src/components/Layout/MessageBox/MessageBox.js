import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {Snackbar, IconButton} from '@material-ui/core/';

import {errorHandlingStore} from '../../../stores/';
import { observer } from "mobx-react"; 

const styles = theme => ({
  close: {
    padding: theme.spacing.unit / 2,
  },
});

class MessageBox extends React.Component {
  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    errorHandlingStore.isShown = false;
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={errorHandlingStore.isShown}
          autoHideDuration={6000}
          onClose={this.handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{errorHandlingStore.message}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleClose}
            >
              X
            </IconButton>
          ]}
        />
      </div>
    );
  }
}

MessageBox.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(observer(MessageBox));

