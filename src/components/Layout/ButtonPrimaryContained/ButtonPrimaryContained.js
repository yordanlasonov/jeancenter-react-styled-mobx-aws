import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';

const ButtonPrimaryContained = (props) => (
  <Button
    type="submit"
    variant="contained"
    color="primary"
    onClick={props.onClick}
  >
    {props.label}
  </Button>
);

ButtonPrimaryContained.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func
};

export default ButtonPrimaryContained;