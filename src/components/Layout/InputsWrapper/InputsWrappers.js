
import React from "react";
import PropTypes from "prop-types";

import styled from "styled-components";

const InputWrapper = styled.div`
  display: inline-block;
  padding:5px;
`;

const InputsWrappers = (props) => (
  <InputWrapper>
    {props.children}
  </InputWrapper>
);

InputsWrappers.propTypes = {
  children: PropTypes.array
};

export default InputsWrappers;