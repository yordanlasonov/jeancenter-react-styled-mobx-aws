
import React from "react";
import PropTypes from "prop-types";

import styled from "styled-components";

const Page = styled.div`
  padding-bottom:50px;
`;

const Heading = styled.div`
  text-align: center;
  font-size: 48px;
  margin-top: 60px;
  transition: 0.3s margin-top;
  @media (max-width: 1120px) {
    margin-top:210px;
  }
  margin-bottom: 20px; 
`;


const StyledPage = (props) => (
  <Page>
    <Heading>{props.title}</Heading>
    {props.children}
  </Page>
);

StyledPage.propTypes = {
  title: PropTypes.string,
  children: PropTypes.array
};

export default StyledPage;