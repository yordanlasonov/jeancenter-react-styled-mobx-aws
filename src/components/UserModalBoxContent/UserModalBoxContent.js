import React, { Component } from 'react';

import { Tabs, Tab } from '@material-ui/core/';

import { languageStore, userProfileStore } from '../../stores';
import { observer } from "mobx-react";

import UserModalBoxProfileContent from '../UserModalBoxProfileContent';
import UserModalBoxAddressesContent from '../UserModalBoxAddressesContent';

const lang = languageStore.language.modalBox;

class UserModalBoxContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: 0
    };

    this.handleTabChange = this.handleTabChange.bind(this);
  }

  handleTabChange = (event, value) => {
    this.setState({
      tab: value
    });
  };

  handleChange(event) {
    userProfileStore.handleChange(event);
  }

  render() {
    return (
      <div>
        <Tabs
          value={this.state.tab}
          indicatorColor='primary'
          textColor='primary'
          centered
          onChange={this.handleTabChange}>
          <Tab label={lang.profileTab} />
          <Tab label={lang.addressTab} />
        </Tabs>

        {(this.state.tab === 0) &&
          <UserModalBoxProfileContent />
        }

        {(this.state.tab === 1) &&
          <UserModalBoxAddressesContent />
        }
      </div>
    );
  }
}

export default observer(UserModalBoxContent);
