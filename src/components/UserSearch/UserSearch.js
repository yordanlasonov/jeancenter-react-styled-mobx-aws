import React from 'react';

import { Input, Select, MenuItem } from '@material-ui/core';
import styled from 'styled-components';

import { languageStore, userSearchStore } from '../../stores/';
import { observer } from "mobx-react";

import UserList from '../UserList';

const lang = languageStore.language.search;

const StyledInput = styled(Input)`
  background-color: white;
  padding:0 5px;
  width:400px;
  @media (max-width: 700px) {
    width:200px;
  }
`;

const InputSelectWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  margin-top:50px;
  margin-bottom:50px;
`;

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  flex-flow: column;
  align-items: center;
  justify-content: center;
`;

const StyledSelect = styled(Select)`
  background-color: #E8E8E8;
  padding: 0 5px 0 15px;
  width: 100px;
`;

class UserSearch extends React.Component {
  constructor(props) {
    super(props);

    this.handleSelectInput = this.handleSelectInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRefetch = this.handleRefetch.bind(this);
  }

  handleSearchInput = (event) => {
    userSearchStore.handleSearchInput(event);
  }

  handleSelectInput(event) {
    userSearchStore.handleSelectInput(event);
    userSearchStore.handleRefetch();
  }

  handleRefetch() {
    userSearchStore.handleRefetch();
  }

  handleSubmit(event) {
    event.preventDefault();
  }


  render() {
    return (
      <StyledForm onSubmit={this.handleSubmit}>
        <InputSelectWrapper>
          <StyledInput
            onChange={this.handleSearchInput}
          />
          <StyledSelect value={userSearchStore.searchOption} onChange={this.handleSelectInput}>
            <MenuItem value="email">{lang.optionEmail}</MenuItem>
            <MenuItem value="familyName">{lang.optionFamilyName}</MenuItem>
            <MenuItem value="loyaltycardnumber">{lang.optionLoyaltyNumber}</MenuItem>
          </StyledSelect>
        </InputSelectWrapper>
        <UserList />
      </StyledForm>
    );
  }
}

export default observer(UserSearch);
