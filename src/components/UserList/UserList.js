import React, { Component } from 'react';

import {languageStore, userSearchStore, modalBoxStore, userProfileStore, userAddressesStore } from '../../stores';
import { observer } from "mobx-react"; 

import UserModalBox from '../UserModalBox';
import UserCard from '../Layout/UserCard';

const lang = languageStore.language.search;

class UserList extends Component {
  
  showUserModal = async (userParam) => {
    await userProfileStore.fetchUserData(userParam);
    await userAddressesStore.fetchUserAddresses(userParam);
    modalBoxStore.show();
  }

  render() {
    let filteredUsers = userSearchStore.searchResult;
    if (filteredUsers && filteredUsers.length > 0) {
      return (
        <React.Fragment>
          {filteredUsers.map((user) => (
            <UserCard 
              user={user}
              key={user.id}
              onClick={() => this.showUserModal(user)}/>
          ))}
          <UserModalBox />
        </React.Fragment>
      );
    } else {
      return <div>{lang.noData}</div>;
    }
  }
}

export default observer(UserList);
