import React from 'react';
import { ConfirmSignIn } from 'aws-amplify-react';
import styled from 'styled-components';

const ConfirmSignInStyled = styled.div`
  text-align:center;
  padding-top:50px;
  a{
    cursor:pointer;
    color: blue;
  }
`;

class MyConfirmSignIn extends ConfirmSignIn {
  constructor(props) {
    super(props);
    this.refreshPage = this.refreshPage.bind(this);
  }
  refreshPage() {
    window.location.reload();
  }
  render() {
    if (this.props.authState !== 'confirmSignUp') return false;
    return (
      <ConfirmSignInStyled>
        You need to be approved to continue. <a onClick={() => this.changeState('signIn')}>Try Again</a>
      </ConfirmSignInStyled>
    );
  }
}

export default MyConfirmSignIn;