import React, { Component } from 'react';

import { languageStore, userSearchStore, userProfileStore, userPasswordStore } from '../../stores';
import { observer } from "mobx-react";

import { InputsWrapper, ButtonsModalBox, FormInput, FormOption, IsActiveUser, LoyaltyCardNumber } from '../Layout';

import genderOptions from '../../lists/gender.json';
import countryOptions from '../../lists/country.json';

const lang = languageStore.language.modalBox.profile;

class UserModalBoxProfileContent extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onSecondaryClick = this.onSecondaryClick.bind(this);
  }

  handleChange(event) {
    userProfileStore.handleChange(event);
  }

  disableUser = async () => {
    await userProfileStore.disableUser();
    userSearchStore.handleRefetch();
  }

  onSecondaryClick() {
    userPasswordStore.resetUserPassword();
  }

  handleSubmit = async () => {
    await userProfileStore.handleSubmit();
    userSearchStore.handleRefetch();
  }

  render() {
    let user = userProfileStore.userProfile;
    return (
      <React.Fragment>
        <InputsWrapper>
          <IsActiveUser enabled={user.enabled} />
          {user.loyaltycardnumber && <LoyaltyCardNumber loyaltycardnumber={user.loyaltycardnumber}/> }
          <FormInput label={lang.firstName} name='name' value={user.name} onChange={this.handleChange} />
          <FormInput label='Email' name='email' value={user.email} onChange={this.handleChange} />
          <FormOption label={lang.country} name='country' value={user.country} options={countryOptions} onChange={this.handleChange} />
        </InputsWrapper>
        <InputsWrapper>
          <FormInput label={lang.familyName} name='familyName' value={user.familyName} onChange={this.handleChange} />
          <FormInput label={lang.phoneNumber} name='phoneNumber' value={user.phoneNumber} onChange={this.handleChange} />
          <FormOption label={lang.gender} name='gender' value={user.gender} options={genderOptions} onChange={this.handleChange} />
        </InputsWrapper>
        <ButtonsModalBox
          submitLabel={lang.submitBtn} 
          dangerLabel={lang.disableBtn} 
          secondaryLabel={lang.passwordBtn}
          onSubmit={this.handleSubmit}
          onDelete={this.disableUser}
          onSecondaryClick={this.onSecondaryClick}
        />
      </React.Fragment>
    );
  }
}

export default observer(UserModalBoxProfileContent);
