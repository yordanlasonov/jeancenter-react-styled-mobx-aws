import React, { Component } from 'react';
import styled from 'styled-components';

import { Paper, Button } from '@material-ui/core/';

import { FormInput } from '../Layout/';

import { languageStore, authStore, superAdminStore } from '../../stores';
import { observer } from "mobx-react";

const lang = languageStore.language.superAdminPanel;

const StyledPaper = styled(Paper)`
  position: absolute;
  top: 80px;
  right: 10px;
  width: 250px;
  padding: 15px 20px 20px;
  h3{
    margin-top:0px;
    text-align:center;
  }
  label{
    display:block;
    margin-bottom:10px;
  }
  input{
    width:100%;
  }
  button{
    margin-top:20px;
  }
`;

class SuperAdminPanel extends Component {
  handleChange(event) {
    superAdminStore.handleChange(event);
  }
  
  handleSubmit(){
    superAdminStore.addNewAdmin();
  }


  render() {
    return (
      <React.Fragment>
        {authStore.isSuperAdmin && 
          <StyledPaper>
            <h3>{lang.title}</h3>
            <FormInput label="E-mail" name='email' onChange={this.handleChange} />
            <Button type='submit' variant='contained' color='primary' onClick={this.handleSubmit}>
              {lang.approveBtn}
            </Button>
          </StyledPaper>
        } 
      </React.Fragment>
    );
  }
}

export default observer(SuperAdminPanel);