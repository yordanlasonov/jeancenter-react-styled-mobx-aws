import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {InputsWrapper, ButtonsModalBox, FormInput, FormOption, FormCheckbox } from '../Layout/';

import { languageStore, userAddressesStore } from '../../stores';
import { observer } from "mobx-react"; 

import genderOptions from '../../lists/gender.json';
import countryOptions from '../../lists/country.json';

const lang = languageStore.language.modalBox.addresses;

class UserModalBoxAddresses extends Component {
  static propTypes = {
    index: PropTypes.number
  }
  constructor(props) {
    super(props);
    
    this.handleChange = this.handleChange.bind(this);
  };

  handleSubmit = async () => {
    await userAddressesStore.handleSubmit(this.props.index);
    userAddressesStore.refetchUserAddresses(this.props.index);
  }

  handleDelete = async () => {
    await userAddressesStore.deleteAddress(this.props.index);
    userAddressesStore.refetchUserAddresses(this.props.index);
  }

  handleChange(event) {
    userAddressesStore.handleChange(event, this.props.index);
  }

  render() {
    let address = userAddressesStore.userAddresses[this.props.index];
    return (
      <div>
        <InputsWrapper>
          <FormCheckbox label={lang.defaultAddress} name='defaultAddress' value={address.defaultAddress} onChange={this.handleChange}/>
          <FormInput label={lang.firstName} name='name' value={address.name} onChange={this.handleChange} />
          <FormInput label={lang.familyName} name='familyName' value={address.familyName} onChange={this.handleChange} />
          <FormInput label={lang.phoneNumber} name='phoneNumber' value={address.phoneNumber} onChange={this.handleChange} />
          <FormOption label={lang.country} name='country' value={address.country} options={countryOptions} onChange={this.handleChange} />
          <FormOption label='Gender' name='gender' value={address.gender} options={genderOptions} onChange={this.handleChange} />
        </InputsWrapper>
        <InputsWrapper>
          <FormInput label={lang.city} name='city' value={address.city} onChange={this.handleChange} />
          <FormInput label={lang.streetName} name='streetName' value={address.streetName} onChange={this.handleChange} />
          <FormInput label={lang.houseNumber} name='houseNumber' value={address.houseNumber} onChange={this.handleChange} />
          <FormInput label={lang.postalCode} name='postalCode' value={address.postalCode} onChange={this.handleChange} />
          <FormInput label={lang.addition} name='addition' value={address.addition} onChange={this.handleChange} />
        </InputsWrapper>
        <ButtonsModalBox
          submitLabel={lang.updateBtn}
          dangerLabel={lang.deleteBtn}
          onSubmit={this.handleSubmit}
          onDelete={this.handleDelete}
        />
      </div>
    );
  }
}

export default observer(UserModalBoxAddresses);