// import awsDev from './aws/awsDev';
// import awsPrd from './aws/awsPrd';

let awsmobile = {};

if (process.env.REACT_APP_ENV === 'integration') {
  //awsmobile = awsDev;
}
else if (process.env.REACT_APP_ENV === 'production') {
  // right now the awsPrd is replicating the info from awsDev
  //awsmobile = awsPrd;
}

export default awsmobile;