import { observable, action } from 'mobx';

export class errorHandlingStore {

  @observable message = '';
  @observable isShown = false;

  @action setMessage = (message) => {
    this.message = message;
    this.isShown = true;
  }
}

export default new errorHandlingStore();
