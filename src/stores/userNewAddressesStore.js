import { observable, action } from 'mobx';
import { API } from 'aws-amplify';

import {errorHandlingStore} from './';

import languageStore from './languageStore';
const lang = languageStore.language.alertBox;

export class userNewAddressesStore {
  @observable userNewAddresses= {
    defaultAddress: true,
    gender: 'MALE',
    country: 'NL'
  };

  @observable defaultNewAddresses = {
    defaultAddress: true,
    name: '',
    familyName: '',
    phoneNumber: '',
    city: '',
    country: 'NL',
    streetName: '',
    houseNumber: '',
    postalCode: '',
    addition: '',
    gender: 'MALE'
  };

  @action handleChange = (event) => {
    const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
    this.userNewAddresses[event.target.name] = value;
  }

  @action handleSubmit = async (currentUserId) => {
    let userNewAddresses = this.userNewAddresses;
    try {
      await API.post('userApi', `/users/${currentUserId}/addresses`, {
        body: {
          'defaultAddress': userNewAddresses.defaultAddress,
          'name': userNewAddresses.name,
          'familyName': userNewAddresses.familyName,
          'phoneNumber': userNewAddresses.phoneNumber,
          'city': userNewAddresses.city,
          'country': userNewAddresses.country,
          'streetName': userNewAddresses.streetName,
          'houseNumber': userNewAddresses.houseNumber,
          'postalCode': userNewAddresses.postalCode,
          'addition': userNewAddresses.addition,
          'gender': userNewAddresses.gender
        }
      });
      this.userNewAddresses = this.defaultNewAddresses;
      errorHandlingStore.setMessage(lang.succesfullyAdded);
    }
    catch(err){
      errorHandlingStore.setMessage(await err.response.data.message);
    }
  }
  

}

export default new userNewAddressesStore();
