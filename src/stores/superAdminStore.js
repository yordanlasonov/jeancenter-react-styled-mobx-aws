import { observable, action } from 'mobx';
import { Auth } from 'aws-amplify';
import AWS from 'aws-sdk';

import { errorHandlingStore, authStore } from './';


export class superAdminStore {
  @observable newAdminInput = ' ';

  @action handleChange = (event) => {
    const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
    this.newAdminInput = value;
  }

  @action addNewAdmin = async () => {
    let credentials = await Auth.currentCredentials();

    console.log(credentials);
    AWS.config.update({
      accessKeyId: credentials.accessKeyId,
      secretAccessKey: credentials.secretAccessKey,
      sessionToken: credentials.sessionToken,
      region: credentials._clientConfig.region
    });
    
    
    let params = {
      UserPoolId: await authStore.UserPoolId, 
      Username: this.newAdminInput 
    };
    const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminConfirmSignUp(params, function (err, data) {
      if (err) {
        errorHandlingStore.setMessage(err.message);
      }// an error occurred
      else {    
        errorHandlingStore.setMessage("Successfully Approved!");
      } 
    });
  }

}

export default new superAdminStore();
