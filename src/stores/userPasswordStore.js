import { API } from 'aws-amplify';

import {userProfileStore, errorHandlingStore } from './';

import languageStore from './languageStore';
const lang = languageStore.language.alertBox;

export class UserPasswordStore {

  resetUserPassword = async () => {
    try{
      await API.post('userApi', `/users/${userProfileStore.userProfile.id}/forgotpassword`);
      errorHandlingStore.setMessage(lang.sucessfullyReset);
    }
    catch (err) {
      errorHandlingStore.setMessage(lang.failedReset);
    }
  }

}

export default new UserPasswordStore();
