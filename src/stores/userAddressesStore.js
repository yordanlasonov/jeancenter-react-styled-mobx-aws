import { observable, action } from 'mobx';
import { API } from 'aws-amplify';

import {errorHandlingStore} from './';

import languageStore from './languageStore';
const lang = languageStore.language.alertBox;

export class userAddressesStore {
  @observable userAddresses = [];
  @observable currentUserId = {};

  @action handleChange = (event, index) => {
    const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
    this.userAddresses[index][event.target.name] = value;
  }

  @action fetchUserAddresses = async (userParam) => {
    try {
      this.currentUserId = userParam.id;
      const addressesResponse = await API.get('userApi', `/users/${this.currentUserId}/addresses/`);
      this.userAddresses = await this.sortAddresses(addressesResponse);
    }
    catch(err){
      errorHandlingStore.setMessage(await err.response.data.message);
    }
  }

  @action refetchUserAddresses = async () => {
    try {
      const addressesResponse = await API.get('userApi', `/users/${this.currentUserId}/addresses/`);
      this.userAddresses = this.sortAddresses(addressesResponse);
    }
    catch(err){
      errorHandlingStore.setMessage(await err.response.data.message);
    }
  }

  @action deleteAddress = async (index) => {
    try {
      let userAddresses = this.userAddresses[index];
      await API.del('userApi', `/users/${this.currentUserId}/addresses/${userAddresses.uid}`);
    }
    catch (err) {
      errorHandlingStore.setMessage(await err.response.data.message);
    }
  }

  sortAddresses(array){
    return array.sort((a, b) => a.uid.localeCompare(b.uid));
  }

  @action handleSubmit = async (index) => {
    let userAddresses = this.userAddresses[index];
    try {
      await API.put('userApi', `/users/${this.currentUserId}/addresses/${userAddresses.uid}`, {
        body: {
          'defaultAddress': userAddresses.defaultAddress,
          'name': userAddresses.name,
          'familyName': userAddresses.familyName,
          'phoneNumber': userAddresses.phoneNumber,
          'city': userAddresses.city,
          'country': userAddresses.country,
          'streetName': userAddresses.streetName,
          'houseNumber': userAddresses.houseNumber,
          'postalCode': userAddresses.postalCode,
          'addition': userAddresses.addition,
          'gender': userAddresses.gender
        }
      });
      errorHandlingStore.setMessage(lang.succesfullyUpdate);
    }
    catch (err) {
      errorHandlingStore.setMessage(await err.response.data.message);
    }
  }
}

export default new userAddressesStore();
