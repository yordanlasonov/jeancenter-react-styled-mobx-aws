import { observable, action } from 'mobx';

export class ModalBoxStore {

  @observable isShown = false;
  

  @action show = () => {
    this.isShown = true;
  }
  
  @action close = () => {
    this.isShown = false;
  }

}

export default new ModalBoxStore();
