import { observable } from 'mobx';
import { Auth } from 'aws-amplify';
import jwtDecode from 'jwt-decode';
import awsExport from '../aws-exports';


export class authStore {
  @observable token = [];
  @observable UserPoolId = '';
  @observable isSuperAdmin = '';
  @observable Username = '';
  @observable jwtToken = '';

  getAuth = async () => {
    const jwtToken = await Auth.currentAuthenticatedUser();
    this.jwtToken = jwtToken;
    this.isSuperAdmin = await jwtToken.signInUserSession.idToken.payload['cognito:groups'] !== undefined;
    this.token = jwtDecode(jwtToken.signInUserSession.accessToken.jwtToken);
    this.UserPoolId = awsExport.aws_user_pools_id;
  }
}

export default new authStore();
