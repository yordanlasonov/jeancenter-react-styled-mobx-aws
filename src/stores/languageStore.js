import { observable } from 'mobx';
import nl from '../i18n/nl.json';

export class languageStore {

  @observable language = nl;

}

export default new languageStore();
