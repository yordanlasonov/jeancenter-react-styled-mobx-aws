import { observable, action } from 'mobx';
import { API } from 'aws-amplify';

import {errorHandlingStore} from './';

import languageStore from './languageStore';
const lang = languageStore.language.alertBox;

export class userProfileStore {

  @observable userProfile = {};

  @action handleChange = (event) => {
    const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
    this.userProfile[event.target.name] = value;
  }

  @action fetchUserData = async (userParam) => {
    const profileResponse = await API.get('userApi', `/users/${userParam.id}`);
    this.userProfile = profileResponse;
  }

  @action refetchUserData = async () => { 
    const profileResponse = await API.get('userApi', `/users/${this.userProfile.id}`);
    this.userProfile = profileResponse;
  }

  @action disableUser = async () => {
    try{
      const profileResponse = await API.del('userApi', `/users/${this.userProfile.id}`);
      this.userProfile = profileResponse;
      errorHandlingStore.setMessage(lang.sucessfullyDeactive);
    }
    catch (err){
      errorHandlingStore.setMessage(await err.response.data.message);
    }
  }

  handleSubmit = async () => {
    let userProfile = this.userProfile;
    try{
      await API.put('userApi', `/users/${userProfile.id}`, {
        body: {
          'email': userProfile.email,
          'name': userProfile.name,
          'familyName': userProfile.familyName,
          'phoneNumber': userProfile.phoneNumber,
          'country': userProfile.country,
          'gender': userProfile.gender
        }
      });
      errorHandlingStore.setMessage(lang.succesfullyUpdate);
    }
    catch (err) {
      errorHandlingStore.setMessage(await err.response.data.message);
    }
  }

}

export default new userProfileStore();
