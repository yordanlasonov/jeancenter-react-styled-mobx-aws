import { observable, action } from 'mobx';
import { API } from 'aws-amplify';

export class userSearchStore {

  @observable searchTerm = '';
  @observable searchResult = [];
  @observable searchOption = 'email'

  @action handleSelectInput = (event) => {
    this.searchOption = event.target.value;
  }

  @action handleSearchInput = async (event) => {
    this.searchTerm = event.target.value;
    if(event.target.value.length > 1){
      const response = await API.get('userApi', '/users?searchField=' + this.searchOption + '&searchText=', {
        queryStringParameters: {
          searchField: this.searchOption,
          searchText: event.target.value
        }
      });
      this.searchResult = response;
    }
    else{
      this.searchResult = [];
    }
  }

  @action handleRefetch = async () => {
    const response = await API.get('userApi', '/users?searchField=' + this.searchOption + '&searchText=', {
      queryStringParameters: {
        searchField: this.searchOption,
        searchText: this.searchTerm
      }
    });
    this.searchResult = response;
  }
}

export default new userSearchStore();
