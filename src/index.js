import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'; // v1.x

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#80b93c',
      main: '#80b93c',
      contrastText: '#ffffff'
    },
    secondary: {
      main: '#7f7f7f'
    }
  }
});

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <App />
  </MuiThemeProvider>,
  document.getElementById('root')
);
