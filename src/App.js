import React from 'react';

import './App.css';
import { hot } from 'react-hot-loader';

import Protected from './components/Protected';
import Amplify from 'aws-amplify';

import { I18n } from '@aws-amplify/core';
import aws_exports from './aws-exports';

import { ForgotPassword, RequireNewPassword, SignIn, SignUp, SignOut, withAuthenticator } from 'aws-amplify-react';

import MyVerifyContact from './components/MyVerifyContact';
import MyConfirmSignIn from './components/MyConfirmSignIn';

import languageStore from './stores/languageStore';
const lang = languageStore.language.signIn;

const dict = {
  'en': {
    'Sign in to your account': lang.title,
    'Username': lang.userName,
    'Password': lang.password,
    'Forget your password? ' : lang.forgotPassword,
    'Reset password': lang.resetPassword,
    'No account? ':  lang.noAccount,
    'Create account' : lang.createAccount,
    'Sign In':  lang.signInBtn
  }
};

I18n.putVocabularies(dict);

Amplify.configure(aws_exports);
class App extends React.Component {

  render() {
    return (
      <div>
        <Protected />
      </div>
    );
  }
}

export default hot(module)(withAuthenticator(App, true, [
  <SignIn />,
  <MyConfirmSignIn />,
  <MyVerifyContact />,
  <SignUp />,
  <ForgotPassword />,
  <RequireNewPassword />,
  <SignOut />
]));
