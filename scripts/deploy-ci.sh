#!/bin/sh
env=$1
awsprofile=$2
if [ $# -gt 0 ] && [ $# -lt 3 ]; then
    if [ "${env}" == "test" ]  || [ "${env}" == "dev" ] || [ "${env}" == "acc" ] || [ "${env}" == "int" ] || [ "${env}" == "prd" ]; then
        if [ "${awsprofile}" == "" ]; then
           echo "Invalid parameter Please insert AWS profile to have access"
           exit -1;
        fi
        gitHubBranch=$(git branch | grep \* | cut -d ' ' -f2)
        branchName=$( echo $gitHubBranch |tr '[:upper:]' '[:lower:]'| sed 's/feature\///'|cut -c1-16| sed 's/\(.*[0-9]\)\(.*\)/\1/' )
        read -p "Press (c or C) for create-stack
Press (u or U) for update-stack (c/u):" choice
            case "$choice" in
                c|C ) stackAction="create-stack";;

                u|U ) stackAction="update-stack" ;;

                * ) echo "Invalid Input Press (c or u)" && exit -1;;
            esac

        deployStackCMD="aws cloudformation ${stackAction} \
            --profile ${awsprofile} --template-url https://s3.eu-central-1.amazonaws.com/joggsrv-cf/react-app.yml \
            --stack-name joggsrv-ui-${branchName}  --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM \
            --parameters ParameterKey=ProjectId,ParameterValue=joggsrv-ui-${branchName} \
            ParameterKey=GitHubOAuthToken,ParameterValue=8c71f7c9f20fbfae18fa0a823957be7823b7ddd4 \
            ParameterKey=Stage,ParameterValue=${env} \
            ParameterKey=GitHubUser,ParameterValue=joggroup \
            ParameterKey=GitHubRepository,ParameterValue=joggsrv_ui_users \
            ParameterKey=GitHubBranch,ParameterValue=${gitHubBranch}"

        echo $deployStackCMD
        execute=$($deployStackCMD)

    else
        echo "Invalid parameter Please insert the target Environment [test,dev,acc,int,prod]"
        exit -1;
    fi

else
    echo "Your command line contains no arguments"
    echo "Please insert the: Target Environment & Your AWS cli Profile name"
    exit -1;
fi
